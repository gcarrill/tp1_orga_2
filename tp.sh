#!/bin/bash
#------------------------------------------------------
# PALETA DE COLORES
#------------------------------------------------------
#setaf para color de letras/setab: color de fondo
	red=$(tput setaf 1);
	green=$(tput setaf 2);
	bg_green=$(tput setab 2);
	blue=$(tput setaf 4);
	bg_blue=$(tput setab 4);
	reset=$(tput sgr0);
	bold=$(tput setaf bold);

#------------------------------------------------------
# DIRECTORIO DE TRABAJO
#------------------------------------------------------

proyectoActual=$(pwd);

#------------------------------------------------------
# DISPLAY MENU
#------------------------------------------------------

imprimir_menu () {
    imprimir_encabezado "    __   __  _______  __    _  __   __    \n\t\t   |  |_|  ||       ||  |  | ||  | |  |   \n\t\t   |       ||    ___||   |_| ||  | |  |   \n\t\t   |       ||   |___ |       ||  |_|  |   \n\t\t   |       ||    ___||  _    ||       |   \n\t\t   | ||_|| ||   |___ | | |   ||       |   \n\t\t   |_|   |_||_______||_|  |__||_______|   \n  __   __  _______  __    _  _______  _______  _______  _______  _______  \n |  |_|  ||   _   ||  |  | ||       ||       ||       ||       ||   _   | \n |       ||  |_|  ||   |_| ||    ___||   _   ||  _____||_     _||  |_|  | \n |       ||       ||       ||   | __ |  | |  || |_____   |   |  |       | \n |       ||       ||  _    ||   ||  ||  |_|  ||_____  |  |   |  |       | \n | ||_|| ||   _   || | |   ||   |_| ||       | _____| |  |   |  |   _   | \n |_|   |_||__| |__||_|  |__||_______||_______||_______|  |___|  |__| |__| \n                                                                          ";

    echo -e "\t\t El directorio de trabajo es:";
    echo -e "\t\t $proyectoActual";

    echo -e "\t\t";
    echo -e "\t\t Opciones:";
    echo "";
    echo -e "\t\t\t a.  Compilar programa resolvente cuadraticas";
    echo -e "\t\t\t b.  Compilar programa producto escalar de vector";
    echo -e "\t\t\t c.  Ejecutar programa resolvente cuadraticas";
    echo -e "\t\t\t d.  Ejecutar programa producto escalar de vector";
    echo -e "\t\t\t e.  Ver codigo resolvente cuadraticas";
    echo -e "\t\t\t f.  Ver codigo producto escalar de vector";
    
    echo -e "\t\t\t q.  Salir";
    echo "";
    echo -e "Escriba la opción y presione ENTER";
}

#------------------------------------------------------
# FUNCTIONES AUXILIARES
#------------------------------------------------------

imprimir_encabezado () {
    clear;
    #Se le agrega formato a la fecha que muestra
    #Se agrega variable $USER para ver que usuario está ejecutando
    echo -e "`date +"%d-%m-%Y %T" `\t\t\t\t USERNAME:$USER";
    echo "";
    #Se agregan colores a encabezado
    echo -e "\t\t${bg_blue}${red}${bold}------------------------------------------\t${reset}";
    echo -e "\t\t${bold}${bg_blue}${red}$1\t\t${reset}";
    echo -e "\t\t${bg_blue}${red}${bold}------------------------------------------\t${reset}";
    echo "";
}

esperar () {
    echo "";
    echo -e "Presione enter para continuar";
    read ENTER ;
}

malaEleccion () {
    echo -e "Selección Inválida ..." ;
}

decidir () {
	echo $1;
	while true; do
		echo "desea ejecutar? (s/n)";
    		read respuesta;
    		case $respuesta in
        		[Nn]* ) break;;
       			[Ss]* ) eval $1
				break;;
        		* ) echo "Por favor tipear S/s ó N/n.";;
    		esac
	done
}

#------------------------------------------------------
# FUNCTIONES del MENU
#------------------------------------------------------

a_funcion () {
  	imprimir_encabezado "\t Opción a.  Compilar programa resolvente cuadraticas";
	echo "---------------------------"
    decidir "cd $proyectoActual/source/resolvente; nasm -f elf resolvente.asm -o resolvente.o; gcc -m32 resolvente.c resolvente.o -o ../../bin/resolvente";

    echo "---------------------------"
}

b_funcion () {
   	imprimir_encabezado "\t Opción a.  Compilar programa producto escalar de vector";
	echo "---------------------------"
    decidir "cd $proyectoActual/source/prod_escalar; nasm -f elf prod_escalar.asm -o prod_escalar.o; gcc -m32 prod_escalar.o -o ../../bin/prod_escalar";

    echo "---------------------------"
}

c_funcion () {
   	imprimir_encabezado "\t Opción c.  Ejecutar programa resolvente cuadraticas";
   	echo -e "Ingrese el valor del parametro 'a'"
    read a;
    echo -e "Ingrese el valor del parametro 'b'"
    read b;
    echo -e "Ingrese el valor del parametro 'c'"
    read c;
    decidir "cd $proyectoActual/bin; ./resolvente $a $b $c";
}

d_funcion () {
    imprimir_encabezado "\t Opción d.  Ejecutar programa producto escalar de vector";
    decidir "cd $proyectoActual/bin; ./prod_escalar";
}

e_funcion () {
    imprimir_encabezado "\t Opción e.  Ver codigo resolvente cuadraticas";
    decidir "less $proyectoActual/source/resolvente/resolvente.c; less $proyectoActual/source/resolvente/resolvente.asm"
}

f_funcion () {
	imprimir_encabezado "\t Opción f.  Ver codigo producto escalar de vector";
    decidir "less $proyectoActual/source/prod_escalar/prod_escalar.asm"
}

q_funcion () {
	clear;
	exit;
}

#------------------------------------------------------
# Funciones Mangosta
#------------------------------------------------------

imprimir_presentacion () {

	clear;
	echo -e "${bold}${green}";
	cat $proyectoActual/menu/presentacion;
	sleep 3s;
    clear;
    cat $proyectoActual/menu/banner;
    sleep 3s;
	echo -e "${reset}";
	clear;
}

#------------------------------------------------------
# LOGICA PRINCIPAL
#------------------------------------------------------
imprimir_presentacion;

while  true
do
    # 1. mostrar el menu
    imprimir_menu;
    # 2. leer la opcion del usuario
    read opcion;

    case $opcion in
        a|A) a_funcion;;
        b|B) b_funcion;;
        c|C) c_funcion;;
        d|D) d_funcion;;
        e|E) e_funcion;;
        f|F) f_funcion;;
        q|Q) q_funcion;;

        *) malaEleccion;;
    esac
    esperar;
done
