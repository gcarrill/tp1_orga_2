<div align="center">

# Organización del computador II – UNGS 2do cuatrimestre del 2021

## TP I - Resolvente en ASM
**Gabriel Carrillo**  
_<gabrielcarrillo123@gmail.com>_

</div>

---

El siguiente trabajo corresponde a la resolución de la consigna del trabajo practico __Formula Resolvente__ de la materia __Organización del Computador 2__.

Este informe se dividirá en las siguientes partes: 
1. [Presentación y uso del menú.](#seccion1)
1. [Observaciones sobre el código.](#seccion2)
    1. [Formula resolvente.](#seccion2.1)
    1. [Producto por escalar de un vector.](#seccion2.2)
1. [Conclusiones y observaciones sobre el trabajo realizado.](#seccion3)

---

## 1 - Presentación y uso del menú.<a name="seccion1"></a>

Dentro del repositorio se encuentra un script de bash de nombre __tp.bash__ que al ejecutarlo presentara el siguiente menú

<div align="center">![Menu](./image/menu.png)</div>

Donde usando las letras de la __"a"__ a la __"f"__ se podrá elegir entre las distintas opciones de este; y con la letra __"q"__ se sale del menú.

Todas las opciones salvo la de salir del menú presentan un mensaje donde se muestra el comando que se ejecutara y pide la confirmación del usuario. 
Por ejemplo:

<div align="center">![Decicion](./image/decicion.png)</div>

Para un correcto uso de las aplicaciones que se dispone, primero hay que ejecutar la opción __"a"__ y la opción __"b"__ para compilarlas.

En el caso de la __resolvente de cuadráticas__, al ejecutarla nos pedirá que se ingresen los parámetros __a__, __b__ y __c__ del polinomio en este orden.

<div align="center">![Resolvente1](./image/resolvente1.png)</div>

Y nos dará el resultado de la siguiente forma.

<div align="center">![Resolvente2](./image/resolvente2.png)</div>

En el caso del __producto escalar de un vector__, como este no recibe parámetros, solo se ejecuta en programa el cual imprime en pantalla la información.

<div align="center">![Producto](./image/producto.png)</div>

---

## 2 - Observaciones sobre el código.<a name="seccion2"></a>
---
### 2.1 - Formula resolvente.<a name="seccion2.1"></a>

El código fuente de este programa consta de 2 archivos; un archivo **_resolvente.c_** el cual contiene el código en C, en este se procesan los parámetros recibidos por __CLI__ y se pasan a la función resolvente de assembler. Y un archivo **_resolvente.asm_** el cual contiene el código de assembler de la función resolvente de cuadráticas.

A continuación se mostrara y explicara el código de ambos archivos comenzando con **_resolvente.c_**.

```
#include <stdio.h>
#include <stdlib.h>

extern void resolvente(double a, double b, double c);

int main(int argc, char *argv[]) {

    if (argc != 4) {
        printf("Parametros insuficientes.\nEl comando debe ser de la forma:\nresolvente [parametro a] [parametro b] [parametro c]\n");
        printf("Por ejemplo: resolvente 1.0 6.0 8.0\n");
        return 1;
    } else {
        double a = atof(argv[1]);
        double b = atof(argv[2]);
        double c = atof(argv[3]);
        printf("\nLas raices del polinomio %.3f·x² + %.3f·x + %.3f son:\n", a, b, c);
        resolvente(a, b, c);
        return 0;
    }
}
```
El propósito general de este código es procesar los parámetros recibidos por CLI y pasárselos a la función _resolvente_ de assembler. 
Cabe destacar que la única comprobación que se realiza es que la cantidad de parámetros recibidos sea la correcta. Faltaría agregar comprobación para los casos en que los parámetros ingresados no sean numéricos.

El código del archivo __resolvente.asm__.

```
extern printf

section .data
    a dq 0.0
    b dq 0.0
    c dq 0.0
    con dd 4.0
    result1 dq 0.0
    result2 dq 0.0
    msg db "x1 = %.3f",09 ,"x2 = %.3f", 10, 13, 10, 13, 0

section .text

    global resolvente

    resolvente:

        ;recibimos los parametros de C
        push ebp ;realizamos
        mov ebp, esp ;enter 0, 0

        mov eax, [ebp + 8] ;extraemos el parametro "a"
        mov ebx, [ebp + 12]
        mov [a], eax ;y lo guardamos en "a"
        mov [a+4], ebx
        mov eax, [ebp + 16] ;extraemos el parametro "b"
        mov ebx, [ebp + 20]
        mov [b], eax ;y lo guardamos en "c"
        mov [b+4], ebx
        mov eax, [ebp + 24] ;extraemos el parametro "c"
        mov ebx, [ebp + 28] 
        mov [c], eax ;y lo guardamos en "c"
        mov [c+4], ebx

        ;calcula 2a
        fld qword[a]
        fadd st0, st0
        ;calcula 4ac
        fld qword[a]
        fld qword[c]
        fmul st0, st1
        fld dword[con] 
        fmul st0, st1
        ;calcula b^2 
        fld qword[b]
        fmul st0, st0
        ;calcula sqrt(b^2-4ac)
        fsub st0, st1
        fsqrt
        ;calculamos 1ra raiz
        fld qword[b]
        fchs
        fadd st0, st1
        fdiv st0, st5
        ;y la guardamos
        fst qword[result1]
        ;calculamos 2da raiz
        fld qword[b]
        fchs
        fsub st0, st2
        fdiv st0, st6
        ;y la guardamos
        fst qword[result2]
        ;imprimimos las raices por consola
        push dword[result2+4]
        push dword[result2]
        push dword[result1+4]
        push dword[result1]
        push msg
        call printf
        add esp, 20

        ;y realizamos leave
        mov ebp,esp
        pop ebp
        ret
```

Cabe aclarar que en esta función no se utilizaron subrutinas para realizar los cálculos intermedios en la FPU, sino que estos se ordenaron de forma que en ningún momento se excediera el limite de la pila del FPU.
Una vez se obtienen las 2 raíces del polinomio, estas se imprimen en pantalla.

---

### 2.2 - Producto escalar de un vector.<a name="seccion2.2"></a>

El código fuente de este programa se encuentra en el archivo **_prod_escalar.asm_**. En este caso el programa no recibe parámetros sino que realiza la multiplicación de un vector ya definido por un valor también ya definido en el código. 
Para este programa si se realizaron subrutinas que se encargan de distintos aspectos. 
A continuación el código fuente: 

```
extern printf

section .data

    str_r db "r = %.3f ", 10, 13, 0
    str_vec db "v = [", 0
    str_val db "%.3f, ", 0
    str_vec_end db "]",10, 13, 0
    str_rvec db "r*v = [", 0

    puntero dq 25.0, 5.0, 4.0, 3.0
    n dq 2.0
    cont dd 4

section .text
    global main

    main:
        mov ebp, esp; for correct debugging
        
        ;imprimimos el valor de r
        push dword [n + 4] ;pusheamos el valor de r
        push dword [n] ;en 2 paretes
        push str_r ;pusheamos el string con formato
        call printf ;llamamos a printf
        add esp, 12 ;restauramos el stack pointer
        
        ;imprimimos la cabecera del vector
        push str_vec ;pusheamos el string con formato
        call printf ;llamamos a printf
        add esp, 4 ;restauramos el stack pointer
        
        ;imprimimos los valores del vector
        push dword [cont] ;pusheamos el largo del vector
        push puntero ;pusheamos el puntero al vector
        push str_val ;pusheamos el string con formato
        push str_vec_end ;y el string del final del vector
        call print_vec ;llamamos a la subrutina que imprime los valores del vector
        add esp, 16 ;restauramos el stack pointer

        ;multiplicamos los valores del vector por r
        push dword [cont] ;pusheamos el largo del vector 
        push puntero ;pusheamos el puntero al vector
        push dword [n + 4] ;pusheamos el valor de r
        push dword [n] ;en 2 paretes
        call producto_rvf ;llamamos a la subrutina que multiplica los valores del vector por r
        add esp, 16 ;restauramos el stack pointer
        
        ;imprimimos la cabecera del vector resultante
        push str_rvec ;pusheamos el string con formato
        call printf ;llamamos a printf
        add esp, 4 ;restauramos el stack pointer
        
        ;imprimimos los valores del vector resultante
        push dword [cont] ;pusheamos el largo del vector
        push puntero ;pusheamos el puntero al vector
        push str_val ;pusheamos el string con formato
        push str_vec_end ;y el string del final del vector
        call print_vec ;llamamos a la subrutina que imprime los valores del vector
        add esp, 16 ;restauramos el stack pointer
    
        ;retornamos
        ret

    ;subrutina que realiza el producto escalar del vector 
    producto_rvf:
        push ebp ;realizamos un 
        mov ebp, esp ;enter 0,0
        mov ebx, [ebp + 16] ;guardamos en ebx el valor del puntero al vector
        fld qword [ebp + 8] ;cargamos en la FPU el valor de r
        mov eax, [ebp + 20] ;guardamos en eax el largo del vector
        
        ;iteramos sobre el vector
        it:
            cmp eax, 0 ;comparamos eax con 0
            jz salir ;si son iguales saltamos a salir
            dec eax ;sino decrementamos eax
            fld qword [ebx] ;guardamos el valor del vector en el tope del FPU
            fmul st0, st1 ;lo multiplicamos por r
            fstp qword [ebx] ;actualizamos el valor del vector y popeamos el valor de la pila del FPU

            add ebx, 8 ;avanzamos al siguiente valor del vector
            jmp it ;y volvemos a iniciar el loop

        salir: ;una vez terminamos de recorrer el vector
            mov ebp,esp ;realizamos leave
            pop ebp 
            ret ;y retornamos

    ;subrutina que imprime los valores del vector
    print_vec:
        push ebp ;realizamos un
        mov ebp, esp ;enter 0,0
        mov ebx, [ebp + 16] ;guardamos en ebx el valor del puntero al vector
        mov eax, [ebp + 20] ;guardamos en eax el largo del vector
        
        ;iteramos sobre el vector
        itp:
            cmp eax, 0 ;comparamos eax con 0
            jz salirp ;si son iguales saltamos a salir
            dec eax ;sino decrementamos eax
            push eax ; y lo pusheamos al stack
            push dword [ebx + 4] ;pusheamos el valor a imprimir del vector
            push dword [ebx] 
            push dword [ebp + 12] ;pusheamos el string con formato
            call printf ;llamamos a printf
            add esp, 12 ;y restauramos el stack pointer
            mov eax, [esp] ;restauramos el contador de elementos del vector
            add esp, 4 ;y restauramos el stack pointer

            add ebx, 8 ;apuntamos al siguiente elemento del vector
            jmp itp ;y volvemos a iniciar el loop

        salirp: ;una vez terminamos de recorrer el vector
            push dword [ebp + 8] ;pusheamos el corchete de cierre del vector 
            call printf ;llamamos a printf
            add esp, 4 ;y restauramos el stack pointer
            mov ebp,esp ;realizamos leave
            pop ebp 
            ret ;y retornamos
```

Al inicio del programa se imprime el valor del escalar **_r_** y el vector **_v_**. Para imprimir el vector **_v_** se llama a la subrutina **_print_vec_** la cual itera sobre los valores de **_v_** y llama a la función **_printf_** de C. Luego se llama a la subrutina **_producto_rvf_**, la cual realiza la multiplicación de **_r_** y los distintos valores de **_v_** y guarda el resultado en **_v_**, actualizando el valor de este. Finalmente se vuelve a llamar a la subrutina **_print_vec_** para imprimir el vector resultante **_r*v_**.

---

## 3 - Conclusiones y observaciones sobre el trabajo realizado.<a name="seccion3"></a>

+ Todo el trabajo fue realizado en Linux y esta pensado para ser ejecutado en este. No se garantiza su correcto funcionamiento en otros sistemas.
+ La mayor dificultad a la hora de realizar el trabajo fue recordar y familiarizarme nuevamente con el set de instrucciones básico de IA32, ya que transcurrieron varios años desde que curse _Organización del Computador I_.
+ A la hora de realizar el script de bash, se opto por reutilizar uno correspondiente a un trabajo practico de la materia _SOR I_ el cual contaba con un menú interactivo.

