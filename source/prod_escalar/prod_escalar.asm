extern printf

section .data

str_r db "r = %.3f ", 10, 13, 0
str_vec db "v = [", 0
str_val db "%.3f, ", 0
str_vec_end db "]",10, 13, 0
str_rvec db "r*v = [", 0

puntero dq 25.0, 5.0, 4.0, 3.0
n dq 2.0
cont dd 4

section .text
global main

main:
    mov ebp, esp; for correct debugging
    
    ;imprimimos el valor de r
    push dword [n + 4]  ;pusheamos el valor de r
    push dword [n]      ;en 2 paretes
    push str_r          ;pusheamos el string con formato
    call printf         ;llamamos a printf
    add esp, 12         ;restauramos el stack pointer
    
    ;imprimimos la cabecera del vector
    push str_vec        ;pusheamos el string con formato
    call printf         ;llamamos a printf
    add esp, 4          ;restauramos el stack pointer
    
    ;imprimimos los valores del vector
    push dword [cont]   ;pusheamos el largo del vector
    push puntero        ;pusheamos el puntero al vector
    push str_val        ;pusheamos el string con formato
    push str_vec_end    ;y el string del final del vector
    call print_vec      ;llamamos a la subrutina que imprime los valores del vector
    add esp, 16         ;restauramos el stack pointer

    ;multiplicamos los valores del vector por r
    push dword [cont]   ;pusheamos el largo del vector    
    push puntero        ;pusheamos el puntero al vector
    push dword [n + 4]  ;pusheamos el valor de r
    push dword [n]      ;en 2 paretes
    call producto_rvf   ;llamamos a la subrutina que multiplica los valores del vector por r
    add esp, 16         ;restauramos el stack pointer
    
    ;imprimimos la cabecera del vector resultante
    push str_rvec       ;pusheamos el string con formato
    call printf         ;llamamos a printf
    add esp, 4          ;restauramos el stack pointer
    
    ;imprimimos los valores del vector resultante
    push dword [cont]   ;pusheamos el largo del vector
    push puntero        ;pusheamos el puntero al vector
    push str_val        ;pusheamos el string con formato
    push str_vec_end    ;y el string del final del vector
    call print_vec      ;llamamos a la subrutina que imprime los valores del vector
    add esp, 16         ;restauramos el stack pointer
    
    ;retornamos
    ret

;subrutina que realiza el producto escalar del vector    
producto_rvf:
    push ebp            ;realizamos un 
    mov ebp, esp        ;enter 0,0
    
    mov ebx, [ebp + 16] ;guardamos en ebx el valor del puntero al vector
    fld qword [ebp + 8] ;cargamos en la FPU el valor de r
    mov eax, [ebp + 20] ;guardamos en eax el largo del vector
    
    ;iteramos sobre el vector
    it:
    cmp eax, 0          ;comparamos eax con 0
    jz salir            ;si son iguales saltamos a salir
    dec eax             ;sino decrementamos eax
    
    fld qword [ebx]     ;guardamos el valor del vector en el tope del FPU
    fmul st0, st1       ;lo multiplicamos por r
    fstp qword [ebx]    ;actualizamos el valor del vector y popeamos el valor de la pila del FPU

    add ebx, 8          ;avanzamos al siguiente valor del vector
    jmp it              ;y volvemos a iniciar el loop

    salir:              ;una vez terminamos de recorrer el vector
    mov ebp,esp         ;realizamos leave
    pop ebp             
    ret                 ;y retornamos

;subrutina que imprime los valores del vector
print_vec:
    push ebp            ;realizamos un
    mov ebp, esp        ;enter 0,0
    
    mov ebx, [ebp + 16] ;guardamos en ebx el valor del puntero al vector
    mov eax, [ebp + 20] ;guardamos en eax el largo del vector
    
    ;iteramos sobre el vector
    itp:
    cmp eax, 0          ;comparamos eax con 0
    jz salirp           ;si son iguales saltamos a salir
    dec eax             ;sino decrementamos eax
    push eax            ; y lo pusheamos al stack
    
    push dword [ebx + 4]    ;pusheamos el valor a imprimir del vector
    push dword [ebx]    
    push dword [ebp + 12]   ;pusheamos el string con formato
    call printf         ;llamamos a printf
    add esp, 12         ;y restauramos el stack pointer
    
    mov eax, [esp]      ;restauramos el contador de elementos del vector
    add esp, 4          ;y restauramos el stack pointer

    add ebx, 8          ;apuntamos al siguiente elemento del vector
    jmp itp             ;y volvemos a iniciar el loop

    salirp:             ;una vez terminamos de recorrer el vector
    push dword [ebp + 8]    ;pusheamos el corchete de cierre del vector 
    call printf         ;llamamos a printf
    add esp, 4          ;y restauramos el stack pointer
    
    mov ebp,esp         ;realizamos leave
    pop ebp     
    ret                 ;y retornamos
    