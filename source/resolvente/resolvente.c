#include <stdio.h>
#include <stdlib.h>

extern void resolvente(double a, double b, double c);

int main(int argc, char *argv[]) {

    if (argc != 4) {
        printf("Parametros insuficientes.\nEl comando debe ser de la forma:\nresolvente [parametro a] [parametro b] [parametro c]\n");
        printf("Por ejemplo: resolvente 1.0 6.0 8.0\n");
        return 1;
    } else {
        double a = atof(argv[1]);
        double b = atof(argv[2]);
        double c = atof(argv[3]);
        printf("\nLas raices del polinomio %.3f·x² + %.3f·x + %.3f son:\n", a, b, c);
        resolvente(a, b, c);
        return 0;
    }
}