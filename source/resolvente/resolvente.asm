extern printf

section .data
    a dq 0.0
    b dq 0.0
    c dq 0.0
    con dd 4.0
        
    result1 dq 0.0
    result2 dq 0.0
    
    msg db "x1 = %.3f",09 ,"x2 = %.3f", 10, 13, 10, 13, 0

section .text

global resolvente

resolvente:

    ;recibimos los parametros de C
    push ebp            ;realizamos
    mov ebp, esp        ;enter 0, 0

    mov eax, [ebp + 8]  ;extraemos el parametro "a"
    mov ebx, [ebp + 12]
    mov [a], eax        ;y lo guardamos en "a"
    mov [a+4], ebx
    mov eax, [ebp + 16] ;extraemos el parametro "b"
    mov ebx, [ebp + 20]
    mov [b], eax        ;y lo guardamos en "c"
    mov [b+4], ebx
    mov eax, [ebp + 24] ;extraemos el parametro "c"
    mov ebx, [ebp + 28] 
    mov [c], eax        ;y lo guardamos en "c"
    mov [c+4], ebx

    ;calcula 2a
    fld qword[a]
    fadd st0, st0
    
    ;calcula 4ac
    fld qword[a]
    fld qword[c]
    fmul st0, st1
    fld dword[con]    
    fmul st0, st1
    
    ;calcula b^2    
    fld qword[b]
    fmul st0, st0
    
    ;calcula sqrt(b^2-4ac)
    fsub st0, st1
    fsqrt
            
    ;calculamos 1ra raiz
    fld qword[b]
    fchs
    fadd st0, st1
    fdiv st0, st5
    ;y la guardamos
    fst qword[result1]
    
    ;calculamos 2da raiz
    fld qword[b]
    fchs
    fsub st0, st2
    fdiv st0, st6
    ;y la guardamos
    fst qword[result2]
    
    ;imprimimos las raices por consola
    push dword[result2+4]
    push dword[result2]
    push dword[result1+4]
    push dword[result1]
    push msg
    call printf
    add esp, 20

    ;y realizamos leave
    mov ebp,esp
    pop ebp
    ret